from collections import Counter
from operator import itemgetter

with open('input_day3.txt') as f:
    lines = [line.strip() for line in f]
counters = [Counter() for _ in range(len(lines[0]))]

for line in lines:
    for counter, bit in zip(counters, line):
        counter.update(bit)
gamma = int(''.join(c.most_common(1)[0][0] for c in counters), 2)
epsilon = int(''.join(c.most_common(2)[1][0] for c in counters), 2)
power_consumption = gamma * epsilon


def oxygen_generator_rating(lines: list[str], size: int):
    for i in range(size):
        counter = Counter(map(itemgetter(i), lines))
        count_0, count_1 = counter['0'], counter['1']
        bit = '1' if count_1 >= count_0 else '0'
        lines = [line for line in lines if line[i] == bit]
        if len(lines) == 1:
            return int(lines[0], 2)


def co2_scrubber_rating(lines: list[str], size: int):
    for i in range(size):
        counter = Counter(map(itemgetter(i), lines))
        count_0, count_1 = counter['0'], counter['1']
        bit = '1' if count_1 < count_0 else '0'
        lines = [line for line in lines if line[i] == bit]
        if len(lines) == 1:
            return int(lines[0], 2)


life_support_rating = oxygen_generator_rating(lines, len(lines[0])) * co2_scrubber_rating(lines, len(lines[0]))

print(lines)
print(f'Part 1: {power_consumption}')
print(f'Part 2: {life_support_rating}')
