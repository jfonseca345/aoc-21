from typing import NamedTuple


class Beacon(NamedTuple):
    x: int
    y: int
    z: int

    def __sub__(self, other):
        return Beacon(self.x - other.x, self.y - other.y, self.z - other.z)


def parse_beacons(scan):
    return [Beacon(*map(int, beacon.split(','))) for beacon in scan.splitlines()[1:]]


with open('input_day19.txt') as f:
    scans = [parse_beacons(scan) for scan in f.read().split('\n\n')]


# TODO YET