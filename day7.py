def to_int(values: list[str]) -> list[int]:
    return list(map(int, values))


with open('input_day7.txt') as f:
    crab_xs = to_int(f.readline().split(','))

print(crab_xs)
print(f'Part 1: {min(sum(abs(crab_x - pos) for crab_x in crab_xs) for pos in range(max(crab_xs)))}')
print(f'Part 2: {min(sum((n:=abs(crab_x - pos))*(n+1)//2 for crab_x in crab_xs) for pos in range(max(crab_xs)))}')