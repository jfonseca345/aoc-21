from itertools import chain
from collections import Counter
from functools import partial


def to_int(values: list[str]) -> list[int]:
    return list(map(int, values))


with open('input_day5.txt') as f:
    lines = [line.strip() for line in f]
lines = [line.split('->') for line in lines]
lines = [(to_int(coord1.split(',')), to_int(coord2.split(','))) for coord1, coord2 in lines]


def is_right_angle(line, diagonals=False):
    (x1, y1), (x2, y2) = line
    if diagonals:
        return x1 == x2 or y1 == y2 or abs(x1-x2) == abs(y1-y2)
    else:
        return x1 == x2 or y1 == y2


def points(line):
    (x1, y1), (x2, y2) = line
    x1, x2 = min(x1, x2), max(x1, x2)
    y1, y2 = min(y1, y2), max(y1, y2)

    if x1 == x2:
        return ((x1, y) for y in range(y1, y2+1))
    else:
        return ((x, y1) for x in range(x1, x2+1))


stepper = lambda x: -1 if x < 0 else 1


def points2(line):
    (x1, y1), (x2, y2) = line

    if abs(x1 - x2) == abs(y1 - y2):
        stepx = stepper(x2 - x1)
        stepy = stepper(y2 - y1)
        xs = (x for x in range(x1, x2 + stepx, stepx))
        ys = (y for y in range(y1, y2 + stepy, stepy))
        return zip(xs, ys)
    else:
        x1, x2 = min(x1, x2), max(x1, x2)
        y1, y2 = min(y1, y2), max(y1, y2)
        if x1 == x2:
            return ((x1, y) for y in range(y1, y2+1))
        else:
            return ((x, y1) for x in range(x1, x2+1))


score1 = sum(map(lambda p: p >= 2, Counter(chain.from_iterable(map(points, filter(is_right_angle, lines)))).values()))
score2 = sum(map(lambda p: p >= 2, Counter(chain.from_iterable(map(points2, filter(partial(is_right_angle, diagonals=True), lines)))).values()))

print(lines)
print(f'Part 1: {score1}')
print(f'Part 2: {score2}')
