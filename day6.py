from collections import Counter

from functools import reduce


def to_int(values: list[str]) -> list[int]:
    return list(map(int, values))


def step(counter):
    new_counter = Counter()
    for clock, count in counter.items():
        if clock > 0:
            new_counter[clock - 1] += count
        else:
            new_counter[6] += count
            new_counter[8] += count
    return new_counter



def size(counter):
    return counter.total()


with open('input_day6.txt') as f:
    initial = Counter(to_int(f.readline().split(',')))

print(initial)
print(f'Part 1: {size(reduce(lambda acc, _: step(acc), range(80), initial))}')
print(f'Part 2: {size(reduce(lambda acc, _: step(acc), range(256), initial))}')
