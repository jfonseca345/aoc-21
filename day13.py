from functools import reduce
from operator import itemgetter

with open('input_day13.txt') as f:
    dots, folds = f.read().split('\n\n')
    dots = {tuple(map(int, dot.split(','))) for dot in dots.strip().split()}
    folds = (fold.removeprefix('fold along ').split('=') for fold in folds.strip().splitlines())
    folds = [(axis, int(value)) for axis, value in folds]


def fold(dot, axis, value):
    d = dict(zip(('x', 'y'), dot))
    d[axis] = value - abs(d[axis] - value)
    return d['x'], d['y']


def print_dots(dots):
    max_x = max(map(itemgetter(0), dots))
    max_y = max(map(itemgetter(1), dots))
    for y in range(max_y + 1):
        for x in range(max_x+1):
            print('#' if (x, y) in dots else ' ', end='')
        print()

print(dots, folds)
first_fold = folds[0]
print(f'Part 1: {len({fold(dot, *first_fold) for dot in dots})}')

print(f'Part 2:\n')
print_dots(
    reduce(lambda ds, fd: {fold(dot, *fd) for dot in ds}, folds, dots)
)
