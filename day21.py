from collections import Counter
from itertools import cycle, islice, product

with open('input_day21.txt') as f:
    player1, player2 = f.read().splitlines()
    player1 = int(player1.removeprefix('Player 1 starting position:'))
    player2 = int(player2.removeprefix('Player 2 starting position:'))

print(player1)
print(player2)


def play(player1, player2):
    deterministic_die = cycle(range(1, 101))
    score1, score2 = [0] * 2
    rolls = 0
    while True:
        player1 = (player1 + sum(islice(deterministic_die, 3)) - 1) % 10 + 1
        score1 += player1
        rolls += 3
        if score1 >= 1_000:
            break

        player2 = (player2 + sum(islice(deterministic_die, 3)) - 1) % 10 + 1
        score2 += player2
        rolls += 3
        if score2 >= 1_000:
            break

    print(score1, score2, rolls)
    return min(score1, score2) * rolls


print(f'Part 1: {play(player1, player2)}')


def play(player1: int, player2: int):
    wins1, wins2 = [0] * 2
    game = Counter({(player1, 0, player2, 0): 1})
    rolls = Counter(sum(i) for i in product(range(1, 4), repeat=3))
    while game:
        new_game = Counter()
        for g, r in product(game, rolls):
            p1, s1, p2, s2 = g
            p1 = (p1 + r - 1) % 10 + 1
            s1 += p1
            universes = rolls[r] * game[g]
            if s1 >= 21:
                wins1 += universes
            else:
                new_game[(p1, s1, p2, s2)] += universes
        game = new_game

        new_game = Counter()
        for g, r in product(game, rolls):
            p1, s1, p2, s2 = g
            p2 = (p2 + r - 1) % 10 + 1
            s2 += p2
            universes = rolls[r] * game[g]
            if s2 >= 21:
                wins2 += universes
            else:
                new_game[(p1, s1, p2, s2)] += universes
        game = new_game

    return max(wins1, wins2)


print(f'Part2: {play(player1, player2)}')
