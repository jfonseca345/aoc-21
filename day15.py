import heapq
from itertools import product
from functools import cache

with open('input_day15.txt') as f:
    graph = [list(map(int, row)) for row in f.read().splitlines()]


def dijkstra(start, destination):
    @cache
    def virtual_value(x, y):
        i, j = x % max_size, y % max_size
        v = graph[j][i] + y // max_size + x // max_size
        return (v - 1) % 9 + 1

    q = []
    entry_finder = {}
    REMOVED = (-1, -1)

    def add_node(node, priority=0):
        if node in entry_finder:
            remove_node(node)

        entry = [priority, node]
        entry_finder[node] = entry
        heapq.heappush(q, entry)

    def remove_node(node):
        entry = entry_finder.pop(node)
        entry[-1] = REMOVED

    def pop_node():
        while q:
            priority, node = heapq.heappop(q)
            if node is not REMOVED:
                del entry_finder[node]
                return node
        raise KeyError('pop from an empty priority queue')

    dist = {node: float('+inf') for node in product(rw, rh)}
    dist[start] = 0

    for node, d in dist.items():
        add_node(node, d)

    while q:
        try:
            current = pop_node()
        except KeyError:
            break
        for neighbor in neighbors(*current):
            if neighbor in entry_finder:
                nx, ny = neighbor
                dist[neighbor] = min(dist[neighbor], dist[current] + virtual_value(nx, ny))
                add_node(neighbor, dist[neighbor])

        if destination not in entry_finder:
            break
    return dist[destination]


def neighbors(x, y):
    points = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
    return (point for point in points if point[0] in rw and point[1] in rh)


print(graph)

max_size = 100
virtual_size = 100
rw, rh = range(max_size), range(max_size)
start = (0, 0)
destination = (virtual_size-1, virtual_size-1)
print(f'Part 1: {dijkstra(start, destination)}')

max_size = 100
virtual_size = max_size * 5
rw, rh = range(virtual_size), range(virtual_size)
start = (0, 0)
destination = (virtual_size-1, virtual_size-1)
print(f'Part 2: {dijkstra(start, destination)}')



