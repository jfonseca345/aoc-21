easy_lens = {2, 3, 4, 7}


def count_easy_numbers(digit):
    return len(digit) in easy_lens


def map_digits(probe):
    m = {8: frozenset(next(p for p in probe if len(p) == 7)), 7: frozenset(next(p for p in probe if len(p) == 3)),
         4: frozenset(next(p for p in probe if len(p) == 4)), 1: frozenset(next(p for p in probe if len(p) == 2))}
    m[6] = frozenset(next(p for p in probe if len(p) == 6 and len(set(p) & m[1]) == 1))
    m[0] = frozenset(next(p for p in probe if len(p) == 6 and set(p) != m[6] and len(set(p) & m[4]) == 3))
    m[9] = frozenset(next(p for p in probe if len(p) == 6 and set(p) != m[6] and set(p) != m[0]))

    m[3] = frozenset(next(p for p in probe if len(p) == 5 and len(set(p) & m[7]) == 3))
    m[2] = frozenset(next(p for p in probe if len(p) == 5 and len(set(p) & m[4]) == 2))
    m[5] = frozenset(next(p for p in probe if len(p) == 5 and set(p) != m[3] and set(p) != m[2]))

    return dict((v, k) for k, v in m.items())


def discover(mapping, numbers):
    return int(''.join(str(mapping[frozenset(number)]) for number in numbers))


with open('input_day8.txt') as f:
    segments = [line.split('|') for line in f]
    digits = [(probe.strip().split(), numbers.strip().split()) for probe, numbers in segments]

print(digits)
print(f'Part 1: {sum(map(count_easy_numbers, sum((numbers for _, numbers in digits), start=[])))}')
print(f'Part 2: {sum(discover(map_digits(probe), numbers) for probe, numbers in digits)}')