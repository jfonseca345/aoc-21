from functools import reduce
from typing import Iterable
from collections import deque
from statistics import median

pairs = {'(': ')', '[': ']', '{': '}', '<': '>'}
syntax_scores = {')': 3, ']': 57, '}': 1197, '>': 25137}
auto_complete_scores = {')': 1, ']': 2, '}': 3, '>': 4}


def syntax_check(program: Iterable[str]):
    stack = deque()
    for char in program:
        if char in pairs:
            stack.append(char)
        else:
            last = stack.pop()
            if pairs[last] != char:
                return syntax_scores[char]
    return 0


def auto_complete(program: Iterable[str]):
    stack = deque()
    for char in program:
        if char in pairs:
            stack.append(char)
        else:
            last = stack.pop()
            if pairs[last] != char:
                return None
    return reduce(lambda score, c: score*5 + auto_complete_scores[pairs[c]], reversed(stack), 0)


with open('input_day10.txt') as f:
    programs = [line.strip() for line in f]


print(programs)
print(f'Part 1: {sum(map(syntax_check, programs))}')
print(f'Part 2: {median(filter(lambda x: x is not None, map(auto_complete, programs)))}')
