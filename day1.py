from more_itertools import sliding_window as sw


def measure(heights: list[int], size: int):
    current_measures = sw(heights[1:], size)
    previous_measures = sw(heights, size)
    increased = lambda current, previous: sum(current) > sum(previous)
    count_if = lambda it: sum(1 if item else 0 for item in it)
    return count_if(map(increased, current_measures, previous_measures))


with open('input_day1.txt') as f:
    lines = [int(line) for line in f.readlines()]
print(f'Part 1: {measure(lines, 1)}')
print(f'Part 2: {measure(lines, 3)}')
