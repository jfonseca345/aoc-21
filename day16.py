import operator
from functools import singledispatch
from itertools import repeat
from math import prod

with open('input_day16.txt') as f:
    transmission = f.read().strip()

print(transmission)


class Packet:
    def __init__(self, version, type_id, contents):
        self.version = version
        self.type_id = type_id
        self.contents = contents

    def __repr__(self):
        return f'Packet<version={self.version}, type_id={self.type_id}, contents={self.contents}>'


def translate_hexa(hexa):
    return f'{int(hexa, 16):04b}'


def feed(carry, bits, amount):
    if len(carry) >= amount:
        return carry[:amount], carry[amount:], bits
    else:
        return feed(carry + translate_hexa(bits[0]), bits[1:], amount)


def parse_header_data(carry, bits):
    data, carry, bits = feed(carry, bits, 3)
    return int(data, 2), carry, bits


def parse_literal(carry, bits):
    def mount_literal(literal, carry, bits):
        group, carry, bits = feed(carry, bits, 5)
        if group[0] == '1':
            return mount_literal(literal + group[1:], carry, bits)
        else:
            return int(literal + group[1:], 2), carry, bits

    return mount_literal('', carry, bits)


def parse_subpackets(carry, bits='', n=None):
    contents = []
    repetition = repeat(None) if n is None else repeat(None, n)
    for _ in repetition:
        if not (carry or bits):
            break
        content, carry, bits = parse_packet(carry, bits)
        contents.append(content)
    return contents, carry, bits


def parse_packet(carry, bits):
    version, carry, bits = parse_header_data(carry, bits)
    type_id, carry, bits = parse_header_data(carry, bits)
    content, carry, bits = parse(type_id, carry, bits)
    return Packet(version, type_id, content), carry, bits


def parse_operator(carry, bits):
    length_type_id, carry, bits = feed(carry, bits, 1)
    if length_type_id == '0':
        length_subpacket, carry, bits = feed(carry, bits, 15)
        length_subpacket = int(length_subpacket, 2)
        subpackets, carry, bits = feed(carry, bits, length_subpacket)
        return parse_subpackets(subpackets), carry, bits
    else:
        n_subpackets, carry, bits = feed(carry, bits, 11)
        n_subpackets = int(n_subpackets, 2)
        return parse_subpackets(carry, bits, n_subpackets)


def parse(type_id, carry, bits):
    if type_id == 4:
        return parse_literal(carry, bits)
    else:
        return parse_operator(carry, bits)


def parse_transmission(transmission):
    contents, carry, bits = parse_subpackets('', transmission, 1)
    return contents[0], carry, bits


# print(parse_transmission('D2FE28'))
# print(parse_transmission('38006F45291200'))
# print(parse_transmission('EE00D40C823060'))
# print()
# print(parse_transmission('8A004A801A8002F478'))
# print(parse_transmission('620080001611562C8802118E34'))
# print(parse_transmission('C0015000016115A2E0802F182340'))
# print(parse_transmission('A0016C880162017C3686B18A3D4780'))


@singledispatch
def version_sum(content):
    pass


@version_sum.register
def _(content: int):
    return 0


@version_sum.register
def _(content: tuple):
    return version_sum(content[0])


@version_sum.register
def _(content: list):
    return sum(version_sum(c) for c in content)


@version_sum.register
def _(content: Packet):
    return content.version + version_sum(content.contents)


# print(version_sum(parse_transmission('8A004A801A8002F478')))
# print(version_sum(parse_transmission('620080001611562C8802118E34')))
# print(version_sum(parse_transmission('C0015000016115A2E0802F182340')))
# print(version_sum(parse_transmission('A0016C880162017C3686B18A3D4780')))

assert version_sum(parse_transmission(transmission)) == 891
print(f'Part 1: {version_sum(parse_transmission(transmission))}')


@singledispatch
def evaluate(content):
    pass


@evaluate.register
def _(content: int):
    return content


@evaluate.register
def _(content: tuple):
    return evaluate(content[0])


@evaluate.register
def _(content: list):
    return [evaluate(c) for c in content]


op_table = {
    0: sum,
    1: prod,
    2: min,
    3: max,
    4: lambda x: x,
    5: lambda args: int(operator.gt(*args)),
    6: lambda args: int(operator.lt(*args)),
    7: lambda args: int(operator.eq(*args)),
}


@evaluate.register
def _(content: Packet):
    op = op_table[content.type_id]
    return op(evaluate(content.contents))


# print()
# print(evaluate(parse_transmission('C200B40A82')))
# print(evaluate(parse_transmission('04005AC33890')))
# print(evaluate(parse_transmission('880086C3E88112')))
# print(evaluate(parse_transmission('CE00C43D881120')))
# print(evaluate(parse_transmission('D8005AC2A8F0')))
# print(evaluate(parse_transmission('F600BC2D8F')))
# print(evaluate(parse_transmission('9C005AC2F8F0')))
# print(evaluate(parse_transmission('9C0141080250320F1802104A08')))

assert evaluate(parse_transmission(transmission)) == 673042777597
print(f'Part 2: {evaluate(parse_transmission(transmission))}')
