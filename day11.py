from itertools import count, product
from typing import Iterable


def to_int(values: Iterable[str]) -> list[int]:
    return list(map(int, values))


rw, rh = range(10), range(10)


def adjacents(x, y):
    points = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
              (x - 1, y - 1), (x - 1, y + 1), (x + 1, y + 1), (x + 1, y - 1)]
    return (point for point in points if point[0] in rw and point[1] in rh)


def step(octopuses):
    to_visit = set()
    for x, y in product(rw, rh):
        octopuses[y][x] += 1
        if octopuses[y][x] > 9:
            to_visit.add((x, y))

    flashed = set()
    while to_visit:
        pos = to_visit.pop()
        if pos in flashed:
            continue
        flashed.add(pos)

        x, y = pos
        for i, j in adjacents(x, y):
            octopuses[j][i] += 1
            if octopuses[j][i] > 9:
                to_visit.add((i, j))

    for x, y in product(rw, rh):
        if octopuses[y][x] > 9:
            octopuses[y][x] = 0

    sync = all(octopuses[y][x] == 0 for x, y in product(rw, rh))
    return len(flashed), sync


with open('input_day11.txt') as f:
    octopuses = [to_int(row.strip()) for row in f]

print(octopuses)
flashes = 0
step_count = 0
for step_count in count(1):
    flashed_count, sync = step(octopuses)

    flashes += flashed_count
    if step_count == 100:
        print(f'Part 1: {flashes}')

    if sync:
        break

print(f'Part 2: {step_count}')

