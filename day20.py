from functools import reduce
from pprint import pprint
import numpy as np
from collections import Counter
from scipy.signal import convolve2d


def enhance(image, infinite_fill, algo):
    kernel = np.array([1 << i for i in range(9)])  # to use convolve2d it is necessary to invert the kernel
    kernel.resize((3, 3))
    new_image = convolve2d(image, kernel, fillvalue=infinite_fill)
    return algo[new_image], algo[infinite_fill * 511]


with open('input_day20.txt') as f:
    algo, image = f.read().split('\n\n')
    algo = np.array(list(map(int, algo.replace('.', '0').replace('#', '1'))))
    image = np.array([list(map(int, row.replace('.', '0').replace('#', '1'))) for row in image.splitlines()])


pprint(algo)
pprint(image)
print(f'Part 1: {Counter(enhance(*enhance(image, 0, algo), algo)[0].flatten())[1]}')
print(f'Part 2: {Counter(reduce(lambda ei, _: enhance(*ei, algo), range(50), (image, 0))[0].flatten())[1]}')