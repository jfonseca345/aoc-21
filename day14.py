from collections import Counter
from functools import reduce
from itertools import pairwise

with open('input_day14.txt') as f:
    template, rules = f.read().split('\n\n')
rules = (rule.split(' -> ') for rule in rules.splitlines())
rules = {i: (i[0] + o, o + i[1]) for i, o in rules}
rules.update({f'{i} ': (f'{i} ',) for i in set(''.join(rules.keys()))})
rules.update({f' {i}': (f' {i}',) for i in set(''.join(rules.keys()))})


def transform(polymer: Counter):
    new = Counter()
    for key, value in polymer.items():
        for t in rules[key]:
            new[t] += value
    return new

def count_single(polymer: Counter):
    new = Counter()
    for key, value in polymer.items():
        for t in key:
            new[t] += value
    new.pop(' ')
    return new


print(template)
print(rules)
template = f' {template} '
counter = count_single(reduce(lambda t, _: transform(t), range(10), Counter("".join(pair) for pair in pairwise(template))))
(_, max_), *_, (_, min_) = counter.most_common()
print(f'Part 1: {(max_ - min_) // 2}')

counter = count_single(reduce(lambda t, _: transform(t), range(40), Counter("".join(pair) for pair in pairwise(template))))
(_, max_), *_, (_, min_) = counter.most_common()
print(f'Part 2: {(max_ - min_) // 2}')

