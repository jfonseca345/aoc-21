from itertools import starmap
from typing import Iterable
from collections import deque
from math import prod


def to_int(values: Iterable[str]) -> list[int]:
    return list(map(int, values))


def draw_basin_map(basin_map):
    with open('basin_day9.txt', 'w', ) as f:
        f.write('\n'.join(' '.join(map(str, w)) for w in basin_map))


def adjacents(w, h):
    rw, rh = range(w), range(h)

    def adjacent(x, y):
        points = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        return (point for point in points if point[0] in rw and point[1] in rh)

    return adjacent


def height_for(heightmap: list[list[int]]):
    def height_at(x: int, y: int) -> int:
        return heightmap[y][x]

    return height_at


def basin_for(heightmap: list[list[int]]):
    w, h = len(heightmap[0]), len(heightmap)
    basin_id = -1
    basin_map = [[-1] * w for _ in range(h)]
    height_at = height_for(heightmap)
    basin_at = height_for(basin_map)
    basin_sizes = []

    def basin_of(x: int, y: int):
        adjacent_of = adjacents(w, h)
        visited = set()
        q = deque([(x, y)])
        while q:
            point = q.popleft()
            if point in visited:
                continue
            height = height_at(*point)
            if height == 9:
                continue
            visited.add(point)
            q += adjacent_of(*point)
        return visited

    for x in range(w):
        for y in range(h):
            if basin_at(x, y) == -1:
                basin = basin_of(x, y)
                if basin:
                    basin_id += 1
                    basin_sizes.append(len(basin))
                    for i, j in basin:
                        basin_map[j][i] = basin_id

    # draw_basin_map(basin_map)

    return sorted(basin_sizes, reverse=True)


def low_heights(heightmap):
    w, h = len(heightmap[0]), len(heightmap)
    adjacents_of = adjacents(w, h)
    height_at = height_for(heightmap)
    for x in range(w):
        for y in range(h):
            min_adjacent = min(starmap(height_at, adjacents_of(x, y)))
            if (low_height := height_at(x, y)) < min_adjacent:
                yield low_height


def risk_level(height):
    return height + 1


with open('input_day9.txt') as f:
    heightmap = [to_int(row.strip()) for row in f]

print(heightmap)
print(f'Part 1: {sum(map(risk_level, low_heights(heightmap)))}')
print(f'Part 2: {prod(basin_for(heightmap)[:3])}')
