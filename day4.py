from collections import Counter
from operator import itemgetter, attrgetter, mul
from typing import NamedTuple
from functools import reduce


class Cell:
    def __init__(self, value: int):
        self.value = value
        self.marked = False

    def mark_if(self, value: int):
        self.marked = self.marked or self.value == value

    def __repr__(self):
        return 'X' if self.marked else str(self.value)

    def __str__(self):
        return 'X' if self.marked else str(self.value)


def to_int(values: list[str]) -> list[int]:
    return list(map(int, values))


def to_cell(values: list[str]) -> list[Cell]:
    return list(map(Cell, map(int, values)))


def verify_board(board: list[Cell]):
    checks = []
    for i in range(5):
        checks.append(board[i::5])
        checks.append(board[i*5:(i+1)*5])
    return any(all(c.marked for c in cells) for cells in checks)


def call(draw: int, boards: list[list[Cell]]):
    for board in boards:
        for cell in board:
            cell.mark_if(draw)
        if verify_board(board):
            return board
    return False


def call_and_delete(draw: int, boards: list[list[Cell]]):
    for board in boards:
        for cell in board:
            cell.mark_if(draw)
    return [board for board in boards if not verify_board(board)]


with open('input_day4.txt') as f:
    draws = to_int(f.readline().strip().split(','))
    boards = [to_cell(board.split()) for board in f.read().strip().split('\n\n')]
    boards2 = boards[:]

for i, draw in enumerate(draws):
    if (board := call(draw, boards)):
        break

score1 = draw * sum(c.value for c in board if not c.marked)

for i, draw in enumerate(draws):
    if len(boards2 := call_and_delete(draw, boards2)) == 1:
        break

for i, draw in enumerate(draws[i:], start=i):
    if (board := call(draw, boards2)):
        break
score2 = draw * sum(c.value for c in board if not c.marked)


print(draws)
print(boards)
print(score1)
print(score2)
