from typing import NamedTuple, Callable
from functools import reduce
import pytest


class Position(NamedTuple):
    depth: int
    horizontal: int
    aim: int = 0

    @classmethod
    def empty(cls):
        return cls(0, 0)

    def follow_commands(self, table: dict[str, Callable[['Position', int], 'Position']], commands: list[tuple[str, int]]):
        position = self
        for command, value in commands:
            position = table[command](position, value)
        return position

        # def follow_command(position: Position, command_value: tuple[str, int]):
        #     command, value = command_value
        #     return table[command](position, value)
        # reduce(follow_command, commands, self)

    def radar(self):
        return self.depth * self.horizontal


@pytest.fixture
def commands():
    with open('input_day2.txt', 'r') as f:
        commands = (line.split(' ') for line in f)
        return [(command, int(value)) for command, value in commands]


def test_part_1(commands):
    table: dict[str, Callable[['Position', int], 'Position']] = {
            'forward': lambda p, v: p._replace(horizontal=p.horizontal + v),
            'down': lambda p, v: p._replace(depth=p.depth + v),
            'up': lambda p, v: p._replace(depth=p.depth - v),
        }
    assert Position.empty().follow_commands(table, commands).radar() == 1635930


def test_part_2(commands):
    table: dict[str, Callable[['Position', int], 'Position']] = {
        'forward': lambda p, v: p._replace(horizontal=p.horizontal + v,
                                           depth=p.depth + p.aim * v),
        'down': lambda p, v: p._replace(aim=p.aim + v),
        'up': lambda p, v: p._replace(aim=p.aim - v),
    }
    assert Position.empty().follow_commands(table, commands).radar() == 1781819478
