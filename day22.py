import functools
import operator
import sys
from itertools import product, starmap
from math import prod
import time


def timer(func):
    """
        adapted over https://realpython.com/primer-on-python-decorators/#timing-functions
    """
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f'Finished {func.__name__!r} in {run_time:.4f} secs', file=sys.stderr)
        return value
    return wrapper_timer


with open('input_day22.txt') as f:
    commands = f.read().splitlines()
    commands = [c.split(' ') for c in commands]
    commands = [(c, [list(map(int, cr[2:].split('..'))) for cr in ranges.split(',')]) for c, ranges in commands]

print(commands)


def within_cube(rng):
    start, stop = rng
    if start > 50 or stop < -50:
        return range(0)
    else:
        istart = max(start, -50)
        istop = min(stop, 50)
        return range(istart, istop+1)


light_cubes = set()
cmd_table = dict(on=light_cubes.add, off=light_cubes.discard)
for c, ranges in commands:
    xrange, yrange, zrange = map(within_cube, ranges)
    for cube in product(xrange, yrange, zrange):
        cmd_table[c](cube)

print(f'Part 1: {len(light_cubes)}')


class Cuboid:
    def __init__(self, mins, maxes, positive):
        self.mins = list(mins)
        self.maxes = list(maxes)
        self.positive = positive

    @classmethod
    def from_ranges(cls, ranges, positive=True):
        mins = map(operator.itemgetter(0), ranges)
        maxes = map(operator.itemgetter(1), ranges)
        return cls(mins, maxes, positive)

    def __and__(self, other):
        mins = list(map(max, self.mins, other.mins))
        maxes = list(map(min, self.maxes, other.maxes))
        if any(starmap(operator.gt, zip(mins, maxes))):
            return None
        positive = self.positive == other.positive
        return Cuboid(mins, maxes, positive)

    def __neg__(self):
        return Cuboid(self.mins, self.maxes, not self.positive)

    def volume(self):
        return (1 if self.positive else -1) * prod(
            (max_ - min_ + 1) for min_, max_ in zip(self.mins, self.maxes))

    def __repr__(self):
        signal = 'Positive' if self.positive else 'Negative'
        coords = ', '.join(f'{coord}={min_}..{max_}' for min_, max_, coord in zip(self.mins, self.maxes, ("x", "y", "z")))
        return f'{signal}Cuboid<{coords}>'


class Region:
    def __init__(self, cuboids):
        self.cuboids = list(cuboids)

    @timer
    def add(self, cuboid):
        cuboids = filter(None, self.cuboids + [cuboid] + [(-c) & cuboid for c in self.cuboids])
        self.cuboids = list(cuboids)

    @timer
    def discard(self, cuboid):
        cuboids = filter(None, self.cuboids + [(-c) & cuboid for c in self.cuboids])
        self.cuboids = list(cuboids)

    @timer
    def within(self, cuboid):
        cuboids = filter(None, [c & cuboid for c in self.cuboids])
        return Region(cuboids)

    @property
    @timer
    def volume(self):
        return sum(map(Cuboid.volume, self.cuboids))

    def __repr__(self):
        return f'Region<{self.cuboids}>'


light_cubes = Region([])
cmd_table = dict(on=light_cubes.add, off=light_cubes.discard)
for c, ranges in commands:
    cuboid = Cuboid.from_ranges(ranges)
    cmd_table[c](cuboid)

print(f'Part 2: {light_cubes.volume}', flush=True)
print(f'Part 1 again: {light_cubes.within(Cuboid.from_ranges([(-50, 50)] * 3)).volume}', flush=True)
