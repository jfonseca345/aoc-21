from collections import defaultdict

with open('input_day12.txt') as f:
    paths = (row.strip().split('-') for row in f)
    map_ = defaultdict(set)
    for from_, to_ in paths:
        map_[from_].add(to_)
        map_[to_].add(from_)
    for to_ in map_.values():
        to_.discard('start')


def paths_to_end(cave: str, visited: frozenset, can_revisit: bool):
    if cave == 'end':
        return 1
    else:
        next_ = (to_visit for to_visit in map_[cave]
                 if to_visit.isupper() or to_visit not in visited or can_revisit)
        return sum(paths_to_end(to_visit,
                                visited if to_visit.isupper() else visited.union({to_visit}),
                                can_revisit and not (to_visit.islower() and to_visit in visited))
                   for to_visit in next_)


print(map_)
print(f'Part 1: {paths_to_end("start", frozenset(), False)}')
print(f'Part 2: {paths_to_end("start", frozenset(), True)}')
